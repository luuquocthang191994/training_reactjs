class Student {
    id: Number;
    name: String;


    public constructor(id: Number, name: String) {
        this.id = id;
        this.name = name;
    }

    public getName(): String {
        return this.name;
    }

    public setName(name: String) {
        this.name = name;
    }
}

const PI: Number = 3.14;

export {
    PI,
    Student
}